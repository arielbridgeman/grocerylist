#!/usr/bin/env python
import sys
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfdevice import PDFDevice, TagExtractor
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import XMLConverter, HTMLConverter, TextConverter
from pdfminer.cmapdb import CMapDB
from pdfminer.layout import LAParams
from pdfminer.image import ImageWriter

from pyik.performance import cached
import numpy as np
import cStringIO
import json, ast

# mostly taken from pdf2txt.py, see install of PDFMINER for additional options like HTML/XML converison
#@cached
def extractor(input):
    import getopt
    def usage():
        print ('usage: %s [-d] [-p pagenos] [-m maxpages] [-P password] [-o output]'
               ' [-C] [-n] [-A] [-V] [-M char_margin] [-L line_margin] [-W word_margin]'
               ' [-F boxes_flow] [-Y layout_mode] [-O output_dir] [-R rotation]'
               ' [-t text|html|xml|tag] [-c codec] [-s scale]'
               ' file ...' % argv[0])
        return 100
    try:
        (opts, args) = getopt.getopt(input, 'dp:m:P:o:CnAVM:L:W:F:Y:O:R:t:c:s:')
        #print argv[1:]
    except getopt.GetoptError:
        return usage()
    if not args: return usage()
    # debug option
    debug = 0
    # input option
    password = ''
    pagenos = set()
    maxpages = 0
    # output option
    outfile = None
    outtype = None
    imagewriter = None
    rotation = 0
    layoutmode = 'normal'
    codec = 'utf-8'
    pageno = 1
    scale = 1
    caching = True
    showpageno = True
    laparams = LAParams()
    for (k, v) in opts:
        if k == '-d': debug += 1
        elif k == '-p': pagenos.update( int(x)-1 for x in v.split(',') )
        elif k == '-m': maxpages = int(v)
        elif k == '-P': password = v
        elif k == '-o': outfile = v
        elif k == '-C': caching = False
        elif k == '-n': laparams = None
        elif k == '-A': laparams.all_texts = True
        elif k == '-V': laparams.detect_vertical = True
        elif k == '-M': laparams.char_margin = float(v)
        elif k == '-L': laparams.line_margin = float(v)
        elif k == '-W': laparams.word_margin = float(v)
        elif k == '-F': laparams.boxes_flow = float(v)
        elif k == '-Y': layoutmode = v
        elif k == '-O': imagewriter = ImageWriter(v)
        elif k == '-R': rotation = int(v)
        elif k == '-t': outtype = v
        elif k == '-c': codec = v
        elif k == '-s': scale = float(v)
    #
    PDFDocument.debug = debug
    PDFParser.debug = debug
    CMapDB.debug = debug
    PDFResourceManager.debug = debug
    PDFPageInterpreter.debug = debug
    PDFDevice.debug = debug
    #
    rsrcmgr = PDFResourceManager(caching=caching)
    outtype = 'text'
    outfp = file(outfile, 'w') if outfile else cStringIO.StringIO()

    if outtype == 'text':
        device = TextConverter(rsrcmgr, outfp, codec=codec, laparams=laparams,
                               imagewriter=imagewriter)
    else:
        return usage()
    for fname in args:
        fp = file(fname, 'rb')
        interpreter = PDFPageInterpreter(rsrcmgr, device)
        for page in PDFPage.get_pages(fp, pagenos,
                                      maxpages=maxpages, password=password,
                                      caching=caching, check_extractable=True):
            page.rotate = (page.rotate+rotation) % 360
            interpreter.process_page(page)
        if type(outfp.getvalue()) is str:
            outfp = outfp.getvalue().replace("\n", "?").replace("\t", " ").replace(" ", " ")
        return json.dumps(outfp)
        fp.close()
    device.close()
    outfp.close()

def parser(rawdata):
    if type(rawdata) is str:
        length = 0
        init = 0
        while length<(len(rawdata)-250):
            serving     = rawdata.find("Makes", length)
            ingredient  = rawdata.find("Ingredients", length)
            direction   = rawdata.find("Directions", length)
            length      = direction + 14
            ender       = rawdata.find("??", length)
            length      = ender

            Name        = rawdata[init:serving]
            Servings    = rawdata[serving:ingredient]
            Ingredients = rawdata[ingredient:direction]
            Directions  = rawdata[direction:ender]
            init =ender+2
            
            print Name
            print "\n"
            print Servings
            print "\n"
            print Ingredients
            print "\n"
            print Directions
            print "\n"

for page in np.arange(67,70,1):
    data = extractor(["-p","%i"%page,'/home/ariel/Documents/TIU/01_Nutrition_Plan_2015.pdf'])
    parser(data)

# if converting pdf book or like, need to have user either select or confirm if section (i.e. breakfast) starts and ends on a certain page. could autodetect from TOC.
# provide summary of pages imported so that user can either go back and manually add or utilize different part of program to do add-ons (ex. pg 73)
# need to be able to identify page number and pictures together so can pull together later
# would be ideal to parse and store in database already in easy way to format...unsure if should do upstream or here.
# issue with parser is TOO specific to cookbook....needs to be more flexible!